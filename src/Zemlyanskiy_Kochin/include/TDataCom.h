#ifndef INCLUDE_TDATACOM_H_
#define INCLUDE_TDATACOM_H_

// ��������� ����� ����������
enum class Data {
  OK,                    // �� ������
  OUT_OF_RANGE,          // ����� �� �������
  INCORRECT_INCOMING,    // ������������ �������� ������
  NO_RECORD,             // ��� ������
  FULL_TAB,              // ������� �����������
  DOUBLE_REC,            // ��� �������� � ����� ������
};

// TDataCom �������� ����� ������� �������
class TDataCom
{
protected:
	Data RetCode;
	Data SetRetCode(const Data ret) { return RetCode = ret; }
public:
	TDataCom() : RetCode(Data::OK) {}
	Data GetRetCode()
	{
		Data temp = RetCode;
		RetCode = Data::OK;
		return temp;
	}
};

#endif  // INCLUDE_TDATACOM_H_