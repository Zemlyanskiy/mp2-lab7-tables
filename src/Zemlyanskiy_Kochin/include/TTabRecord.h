#ifndef __TRECORD_H
#define __TRECORD_H

#include <iostream>
#include "TDatValue.h"

typedef std::string TKey;

class TTabRecord : public TDatValue {
protected:
	TKey Key; // ���� ������
	PTDatValue pValue; // ��������� �� ��������

public:
	TTabRecord(TKey k = "", PTDatValue pVal = NULL);
	TTabRecord(const TTabRecord&) = delete;
	void SetKey(TKey k);
	TKey GetKey() const;
	void SetValuePtr(PTDatValue p);
	PTDatValue GetValuePtr() const;
	virtual TDatValue* GetCopy();
	TTabRecord& operator=(TTabRecord& tr);
	virtual bool operator==(const TTabRecord& tr);
	virtual bool operator<(const TTabRecord& tr);
	virtual bool operator>(const TTabRecord& tr);

	friend class TArrayTable;
	friend class TScanTable;
	friend class TSortTable;
	friend class TTreeNode;
	friend class TArrayHash;
	friend class TListHash;

};

typedef TTabRecord* PTTabRecord;

#endif