#ifndef TTREETABLE_H_
#define TTREETABLE_H_

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// ��������� �� ������ ������
	PTTreeNode *ppRef;			// ����� ��������� �� �������-���������� � FindRecord
	PTTreeNode pCurrent;		// ��������� �� ������� �������
	int CurrPos;				// ����� ������� �������
	std::stack < PTTreeNode> St;		// ���� ��� ���������
	void DeleteTreeTab(PTTreeNode pNode); // ��������
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// ����������
	
	// �������������� ������
	virtual bool IsFull() const override;						//���������?
	
	//�������� ������
	virtual PTDatValue FindRecord(TKey k) override;				// ����� ������
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// ��������
	virtual void DelRecord(TKey k) override;					// ������� ������
	
	// ���������
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// ���������� �� ������ ������
	virtual bool IsTabEnded(void) const override;		// ������� ���������?
	virtual Data GoNext(void) override;					// ������� � ��������� ������
};

#endif