#include "../gtest/gtest.h"
#include "../include/TSortTable.h""

TEST(TScanTable, Can_Create_ScanTable) {
	EXPECT_NO_FATAL_FAILURE(TScanTable table(10));
}

TEST(TScanTable, Can_Insert_Record) {
	TScanTable* table = new TScanTable(7);
	int i = 7;
	TTabRecord rec("Kochin V.", (TDatValue*)&i);	
	table->InsRecord("Kochin V.", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, Can_Find_Record) {
	TScanTable* table = new TScanTable(5);
	int* i= new int (1);
	i[0] = 5;
	table->InsRecord("Kochin I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Merkel", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Ivanov N.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Petrov L.", (TDatValue*)i);

	EXPECT_EQ(2 , *((int*)(table->FindRecord("Merkel"))));
}

TEST(TScanTable, Can_Delete_Record) {
	TScanTable* table = new TScanTable(5);
	int *i = new int(1);
	i[0] = 5;
    table->InsRecord("Kochin I.", (TDatValue*)&i[0]);
    i = new int(1);
    i[0] = 2;
    table->InsRecord("Merkel", (TDatValue*)&i[0]);
    i = new int(1);
    i[0] = 3;
    table->InsRecord("Ivanov N.", (TDatValue*)i);
    i = new int(1);
    i[0] = 4;
    table->InsRecord("Petrov L.", (TDatValue*)i);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Merkel"));
	EXPECT_TRUE(nullptr == table->FindRecord("Merkel"));
}

TEST(TSortTable, Can_Create_Table) {
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TScanTable, Can_Insert_Two_Records) {
    TScanTable* table = new TScanTable(5);
    int i = 5;
    table->InsRecord("Zemlyanskiy N.", (TDatValue*)&i);
    EXPECT_NO_FATAL_FAILURE(table->InsRecord("Zemlyanskiy A.", (TDatValue*)&i));
}

TEST(TSortTable, Can_Insert_Record) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	TTabRecord rec("Zemlyanskiy N.", (TDatValue*)&i);
	table->InsRecord("Zemlyanskiy N.", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, Can_Assign_Table) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->InsRecord("Kochin I.", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, Can_Set_And_Get_Sort_Method) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, Can_Use_Sort_And_Can_Find_Record) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Veselov K.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Medvedev", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Smirnov N.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Petrov L.", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Medvedev"))));
}
