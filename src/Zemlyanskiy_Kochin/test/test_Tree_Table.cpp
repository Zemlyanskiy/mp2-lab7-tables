#include "../gtest/gtest.h"
#include "../include/TTreeTable.h"
#include "../include/TBalanceTree.h"

TEST(TTreeNode, Can_Create_TTreeNode) {
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TTreeNode, Can_Get_Tree_Branch) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("Ilya", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Ivanov S.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("Jobs", (PTDatValue)&i[0],&left,&right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}

TEST(TBalanceNode, Can_Create_TBalanceNode) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TTreeTable, Can_Create_TTreeTable) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TTreeTable, Can_Insert_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("that", (PTDatValue)&i[0]));
	table->InsRecord("thot", (PTDatValue)&i[0]);
	table->InsRecord("thut", (PTDatValue)&i[0]);
}

TEST(TTreeTable, Can_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("that", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("thot", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("thut", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("thot"), 5);
}

TEST(TBalanceTree, Can_Create_TBalanceTree) {
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TBalanceTree, Can_Add_And_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("phat", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("jhat", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("jhot", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("jhut", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("thut", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("jhot"), 1);
}

